package main

type MyModel struct {
	Index int    `json:"index" struct:"index"`
	Value string `json:"value" struct:"value"`
}

type ErrResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type GooValue struct {
	Max int `json:"max" struct:"max"`
	Min int `json:"-" struct:"min"`
}
