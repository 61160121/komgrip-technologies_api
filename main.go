package main

import "net/http"

var (
	userMux *http.ServeMux
)

func init() {
	userMux = NewMux()
}

func main() {
	http.HandleFunc("/", User)
	http.ListenAndServe(":8080", nil)
}

func User(w http.ResponseWriter, r *http.Request) {

	userMux.ServeHTTP(w, r)

}

//
func NewMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.HandleFunc("/example1", MaxValue)
	mux.HandleFunc("/example2", ConvertToMyModels)

	return mux

}
