package main

import (
	"encoding/json"
	"net/http"
)

func SuccessResponse(w http.ResponseWriter, code int, result interface{}) error {
	SetupHeaderAccess(&w)
	w.WriteHeader(code)
	return WriteResponse(w, result)
}

func SetupHeaderAccess(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Auth")
	(*w).Header().Set("Content-Type", "application/json")
}

func WriteResponse(w http.ResponseWriter, data interface{}) error {
	SetupHeaderAccess(&w)
	resDocJson, err := json.Marshal(data)
	if err != nil {
		return err
	}

	if _, err := w.Write(resDocJson); err != nil {
		return err
	}

	return nil
}

func ErrorResponse(w http.ResponseWriter, code int, message string) error {
	SetupHeaderAccess(&w)
	responseError := ErrResponse{
		Code:    code,
		Message: message,
	}
	w.WriteHeader(code)
	return WriteResponse(w, responseError)
}
