package main

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

//ข้อ1_API
func MaxValue(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodPost:
	default:
		ErrorResponse(
			w,
			http.StatusForbidden,
			"This function use method POST.",
		)
		return
	}

	var value GooValue

	r.ParseForm()

	r.ParseMultipartForm(10 << 20)

	number := strings.TrimSpace(r.PostForm.Get("number"))

	inputStrArr := strings.Split(number, " ")
	// "9", "6", "2", "4", "7", "1", "11"

	fmt.Println("SSS", number)

	var inputNumberArr = []int{}

	//array string to array int
	for _, v := range inputStrArr {

		number, _ := strconv.Atoi(v)

		inputNumberArr = append(inputNumberArr, number)

	}

	fmt.Println("AAA", inputNumberArr)

	//Find Max Number
	resultMax := 0

	for _, v := range inputNumberArr {

		if v > resultMax {

			resultMax = v
		}

	}

	value.Max = resultMax

	fmt.Println("Max number is", value.Max)

	data := map[string]interface{}{
		"data":    value,
		"message": "Success!",
	}

	SuccessResponse(w, http.StatusOK, data)
}

//ข้อ2_API
func ConvertToMyModels(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodPost:
	default:
		ErrorResponse(
			w,
			http.StatusForbidden,
			"This function use method POST.",
		)
		return
	}

	var modelArr []MyModel

	r.ParseForm()

	r.ParseMultipartForm(10 << 20)

	color := strings.TrimSpace(r.PostForm.Get("color"))

	inputColorArr := strings.Split(color, " ")

	for i, v := range inputColorArr {
		var model MyModel
		model.Index = i
		model.Value = v

		modelArr = append(modelArr, model)
		// res1 = append(mymodel.value, v)
	}
	fmt.Println(modelArr)

	fmt.Println(modelArr[0].Index)
	fmt.Println(modelArr[0].Value)

	fmt.Println(modelArr[1].Index)
	fmt.Println(modelArr[1].Value)

	fmt.Println(modelArr[2].Index)
	fmt.Println(modelArr[2].Value)

	data := map[string]interface{}{
		"data":    modelArr,
		"message": "Success!",
	}

	SuccessResponse(w, http.StatusOK, data)
}
